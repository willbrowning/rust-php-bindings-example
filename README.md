# Sample AnonAddy Sequoia PHP bindings

Run with:

```
cargo build && php -dextension=$(pwd)/target/debug/libmylib.so -a
```

Then test by typing:

```
$signing_cert = file_get_contents("signing-key.asc");
$recipient_cert = file_get_contents("wiktor.asc");
echo anonaddy_sequoia_encrypt($signing_cert, $recipient_cert, "Hello Bruno!");
```
