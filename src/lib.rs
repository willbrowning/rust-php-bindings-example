extern crate solder;

use solder::*;
use solder::zend::*;
use solder::info::*;

use std::io::Write;

use anyhow::Context;

use sequoia_openpgp as openpgp;

use openpgp::{Cert, serialize::stream::{Armorer, Signer}};
use openpgp::types::KeyFlags;
use openpgp::parse::Parse;
use openpgp::serialize::stream::{
    Message, LiteralWriter, Encryptor,
};
use openpgp::policy::StandardPolicy as P;


#[no_mangle]
pub extern fn php_module_info() {
    print_table_start();
    print_table_row("Bindings for OpenPGP library Sequoia", "enabled");
    print_table_end();
}

#[no_mangle]
pub extern fn get_module() -> *mut zend::Module {
    let function = FunctionBuilder::new(c_str!("anonaddy_sequoia_encrypt"), sequoia_encrypt)
        .with_arg(ArgInfo::new(c_str!("name"), 0, 0, 0))
        .build();
    ModuleBuilder::new(c_str!("sequoia"), c_str!("0.1.0-dev"))
        .with_info_function(php_module_info)
        .with_function(function)
        .build()
        .into_raw()
}


#[no_mangle]
pub extern fn sequoia_encrypt(_data: &ExecuteData, retval: &mut Zval) {
    let mut signing_cert_zval = Zval::new_as_null();
    let mut recipient_cert_zval = Zval::new_as_null();
    let mut content_zval = Zval::new_as_null();

    php_parse_parameters!(&mut signing_cert_zval, &mut recipient_cert_zval, &mut content_zval);

    let signing_cert = String::try_from(signing_cert_zval).ok().unwrap();
    let content = String::try_from(content_zval).ok().unwrap();
    let recipient_cert = String::try_from(recipient_cert_zval).ok().unwrap();

    let hello = encrypt_for(signing_cert, recipient_cert, content).unwrap_or(String::from("something bad happened"));

    //let hello = format!("Hello {}, hello{} !", cert, content);
    php_return!(retval, hello);
}


fn encrypt_for(signing_cert: String, recipient_cert: String, content: String) -> openpgp::Result<String> {
    let p = &P::new();

    let mode = KeyFlags::empty().set_storage_encryption().set_transport_encryption();

    let signing_cert = Cert::from_bytes(&signing_cert)?
    .keys().unencrypted_secret()
    .with_policy(p, None).alive().revoked(false).for_signing()
    .nth(0).unwrap().key().clone().into_keypair()?;


    // Read the certificates from the given files.
    let certs: Vec<openpgp::Cert> = vec![Cert::from_bytes(&recipient_cert)?];

    // Build a list of recipient subkeys.
    let mut recipients = Vec::new();
    for cert in certs.iter() {
        // Make sure we add at least one subkey from every
        // certificate.
        let mut found_one = false;
        for key in cert.keys().with_policy(p, None)
            .alive().revoked(false).key_flags(&mode)
        {
            recipients.push(key);
            found_one = true;
        }

        if ! found_one {
            return Err(anyhow::anyhow!("No suitable encryption subkey for {}",
                                       cert));
        }
    }

    // Compose a writer stack corresponding to the output format and
    // packet structure we want.
    let mut sink = vec![];

    // Stream an OpenPGP message.
    let message = Message::new(&mut sink);

    let message = Armorer::new(message).build()?;

    // We want to encrypt a literal data packet.
    let message = Encryptor::for_recipients(message, recipients)
        .build().context("Failed to create encryptor")?;

    let message = Signer::new(message, signing_cert).build()?;

    let mut message = LiteralWriter::new(message).build()
        .context("Failed to create literal writer")?;

    // Copy stdin to our writer stack to encrypt the data.
    //io::copy(&mut io::stdin(), &mut message)
    //    .context("Failed to encrypt")?;
    message.write_all(content.as_bytes())?;

    // Finally, finalize the OpenPGP message by tearing down the
    // writer stack.
    message.finalize()?;

    Ok(String::from_utf8(sink)?)
}
